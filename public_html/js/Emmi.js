// Tekijä: Emmi Laine

$(document).ready(function () {
// visan ohjelmointi
// apu arvo, jotta saadaan uudelleen yritys toimimaan
    let arvo = 0;
    // pisteiden lasku nollataan ja tarkistetaan onko vastaus oikein vai väärin + pisteiden lasku ja palkintojen tulostus
    let counter = 0;
    $(".vast").click(function () {
        let vastaus = Number($(this).val());
        let kysymys = $(this).attr("name");
        if (vastaus === 1) {
            $("[name=" + kysymys + "]").prop("disabled", true);
            $("#" + kysymys + "_selitys").show();
            $(this).next().addClass("vastaus");
            $(this).parent().addClass("oikein");
            counter++;
            $("#tassut").append("<i class='fa fa-paw fa-3x' aria-hidden='true'></i> ");
            $(".seuraava").removeClass("esiin");
            $("#" + kysymys + "yrita").hide();
        } else {
            // uudelleen yritys
            arvo++;
            if (arvo < 2) {
                $("[name=" + kysymys + "]").prop("enabled", true);
                $(this).parent().addClass("vaarin");
                $("#" + kysymys + "yrita").show();
            } else {
                $("[name=" + kysymys + "]").prop("disabled", true);
                $("#" + kysymys + "_selitys").show();
                $("#" + kysymys + "yrita").hide();
                $(this).parent().addClass("vaarin");
                $("#tassut").append("<i class='fa fa-paw fa-3x vt' aria-hidden='true'></i> ");
                $("[name=" + kysymys + "][value=1]").next().addClass("oikea");
                $(".seuraava").removeClass("esiin");
            }
        }


        //tulostetaan pistemäärä, titteli pistemäärän mukaan + palkinto pokaali
        $("#pisteet").html(counter);
        if (counter === 3) {
            $("#titteli").html("Olet luontoon tutustuja!");
            $("#palkinto").html("<i class='fa fa-trophy palkinto fa-5x' aria-hidden='true'></i>");
            $("#uudelleen").addClass("esiin");
        } else if (counter <= 10 && counter >= 8) {
            $("#titteli").html("Olet metsänhenki!");
            $("#palkinto").html("<i class='fa fa-trophy palkinto fa-5x' aria-hidden='true'></i>");
            $("#uudelleen").addClass("esiin");
        } else if (counter <= 2) {
            $("#titteli").html("Olet luontoon tutustuja!");
            $("#uudelleen").html("Yritä uudelleen, niin voit saada kultaisen pokaalin.");
        } else {
            $("#titteli").html("Olet metsän haltija!");
            $("#palkinto").html("<i class='fa fa-trophy palkinto fa-5x' aria-hidden='true'></i>");
            $("#uudelleen").addClass("esiin");
        }
    });

    kysymykset2 = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10"];


    $(".seuraava").click(function () {
        arvo = 0;
        // lisätään esiin- class aina uudessa kysymyksessä
        $(".seuraava").addClass("esiin");
        if (kysymykset2.length > 0) {
            let kysymys = getRndInteger(0, kysymykset2.length - 1);
            let esille = kysymykset2[kysymys];
            $(esille).removeClass("piilotettu");
            kysymykset2.splice(kysymys, 1);
            $(".focus").focus();
            $(this).parent().hide();
        } else {
            // piilotetaan viimeinen kysymys ja näytetään loppu sivu
            $(this).parent().hide();
            $("#loppu").show();
        }
    });

    //arvotaan randomit luvut visaan
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }


    //arvotaan randomit desimaali luvut matikka peliin
    function getRndNumber(min, max) {
        return (Math.random() * (max - min) + min);
    }



    // matikkapelin ohjelmointi
    //array desimaaleille
    let Array = [];
    //kun klikataan aloitus painiketta nollataan pisteet + ohje tekstit
    $("#alku").click(function () {
        pisteet = 0;
        $(".next").hide();
        $(".vastaa").hide();
        //tarkistetaan mikä desimaali aihe on valittuna
        let value = Number($("input[name=teht_1]:checked").val());
        sivu = 0;
        // jos value on 0, niin arvotaan randomit desimaalit arrayyn ja lisätään ohje tekstit
        if (value === 0) {
            $("#teksti").html("Pyöristä kokonaisiksi.");
            $(".aloitus").addClass("piilota");
            $("#tehtävät").show();
            for (let i = 0; i < 4; i++) {
                let num = getRndNumber(0, 9).toFixed(2);
                Array.push(num);
                $("#1_numero_1").html(Array[0]);
                $("#1_numero_2").html(Array[1]);
                $("#1_numero_3").html(Array[2]);
                $("#1_numero_4").html(Array[3]);
            }
        }

        // jos value on 1, niin arvotaan randomit desimaalit arrayyn ja lisätään ohje tekstiä
        if (value === 1) {
            $("#teksti").html("Pyöristä kymmenesosien tarkkuudella.");
            $("#teksti").append("<br> Muista käyttää pilkkua ( , ).</br>");
            $(".aloitus").addClass("piilota");
            $("#tehtävät").show();
            for (let i = 0; i < 4; i++) {
                let num = getRndNumber(0, 9).toFixed(3);
                Array.push(num);
                $("#1_numero_1").html(Array[0]);
                $("#1_numero_2").html(Array[1]);
                $("#1_numero_3").html(Array[2]);
                $("#1_numero_4").html(Array[3]);
            }
        }

        // jos value on 2, niin arvotaan randomit desimaalit arrayyn ja lisätään ohje tekstiä
        if (value === 2) {
            $("#teksti").html("Pyöristä sadasosien tarkkuudella.");
            $("#teksti").append("<br> Muista käyttää pilkkua ( , ).</br>");
            $(".aloitus").addClass("piilota");
            $("#tehtävät").show();
            for (let i = 0; i < 4; i++) {
                let num = getRndNumber(0, 9).toFixed(4);
                Array.push(num);
                $("#1_numero_1").html(Array[0]);
                $("#1_numero_2").html(Array[1]);
                $("#1_numero_3").html(Array[2]);
                $("#1_numero_4").html(Array[3]);
            }
        }
    });

    //apu muuttuja jonka avulla lasketaan milloin lopetus sivu tuodaan esille
    let sivu = 0;
    // seuraava sivu esille, piilotetaan vanha + tuodaan lopetus sivu esille, sekä piilotetaan tehtävät
    $(".next").click(function () {
        $("#tarkista").attr("disabled", false);
        $("#correct1").val("");
        $("#correct2").val("");
        $("#correct3").val("");
        $("#correct4").val("");
        $(".next").hide();
        let tyhjat = $("input[type=number]").val();
        if (tyhjat === "") {
            $(".texts").hide();
        }
        $("#1_numero_1").removeClass("oikein");
        $("#1_numero_1").removeClass("vaarin");
        $("#1_numero_2").removeClass("oikein");
        $("#1_numero_2").removeClass("vaarin");
        $("#1_numero_3").removeClass("oikein");
        $("#1_numero_3").removeClass("vaarin");
        $("#1_numero_4").removeClass("oikein");
        $("#1_numero_4").removeClass("vaarin");
        sivu++;
        Array = [];

        // katsotaan, milloin lopetus sivu tuodaan esille
        if (sivu === 2) {
            $("#lopetus").show();
            $("#tehtävät").hide();
            $("#points").html(pisteet);
        }

        // arvotaan peliin numerot ja laitetaan ne arrayhyn
        let value = Number($("input[name=teht_1]:checked").val());
        if (value === 0) {
            for (let i = 0; i < 4; i++) {
                let num = getRndNumber(0, 9).toFixed(3);
                Array.push(num);
                $("#1_numero_1").html(Array[0]);
                $("#1_numero_2").html(Array[1]);
                $("#1_numero_3").html(Array[2]);
                $("#1_numero_4").html(Array[3]);
            }
        } else if (value === 1) {
            for (let i = 0; i < 4; i++) {
                let num = getRndNumber(0, 9).toFixed(4);
                Array.push(num);
                $("#1_numero_1").html(Array[0]);
                $("#1_numero_2").html(Array[1]);
                $("#1_numero_3").html(Array[2]);
                $("#1_numero_4").html(Array[3]);
            }
        } else {
            for (let i = 0; i < 4; i++) {
                let num = getRndNumber(0, 9).toFixed(5);
                Array.push(num);
                $("#1_numero_1").html(Array[0]);
                $("#1_numero_2").html(Array[1]);
                $("#1_numero_3").html(Array[2]);
                $("#1_numero_4").html(Array[3]);
            }
        }

        // arvotaan palkinto kuva kun painetaan seuraava painiketta
        // Kuvien lähde: https://pixabay.com/fi/
        let kuvat1 = ["<img class='kuva img-fluid' src='img/Silta.jpg' alt='silta'/>", "<img class='kuva img-fluid' src='img/Yö.jpg' alt='yo'/>"];
        let kuvat2 = ["<img class='kuva img-fluid' src='img/Kettu.jpg' alt='kettu'/>", "<img class='kuva img-fluid' src='img/Tiikeri.jpg' alt='tiikeri'/>"];
        let kuvat3 = ["<img class='kuva img-fluid' src='img/Huone.jpg' alt='huone'/>", "<img class='kuva img-fluid' src='img/Tausta.png' alt='tausta'/>"];


        // Pisteiden määrän mukaan arvotaan palkinto kuva modaaliin
        if (pisteet >= 6) {
            let luku = getRndInteger(0, 1);
            let kuva = kuvat1[luku];
            $("#kuva").html(kuva);
        } else if (pisteet <= 2) {
            let luku3 = getRndInteger(0, 1);
            let kuva3 = kuvat3[luku3];
            $("#kuva").html(kuva3);
        } else {
            let luku2 = getRndInteger(0, 1);
            let kuva2 = kuvat2[luku2];
            $("#kuva").html(kuva2);
        }
    });


    let pisteet = 0;
    // kun painetaan tarkista painiketta, niin tarkistetaan ovatko vastaukset oikein vai väärin + pisteiden lasku
    $("#tarkista").click(function () {
        // Jos jokin vastaus laatikoista on tyhjä, ilmoitetaan, että kaikkiin tehtäviin pitää vastata
        let va11 = $("input[id=correct1]").val();
        let va22 = $("input[id=correct2]").val();
        let va33 = $("input[id=correct3]").val();
        let va44 = $("input[id=correct4]").val();
        if (va11 === "" || va22 === "" || va33 === "" || va44 === "") {
            $("#tarkista").attr("disabled", false);
            $(".vastaa").show();
            
            $(".tarkki").html("");
            $("#1_numero_1").removeClass("vaarin");
            $("#1_numero_1").removeClass("oikein");
            
            $(".tarkki1").html("");
            $("#1_numero_2").removeClass("vaarin");
            $("#1_numero_2").removeClass("oikein");
            
            $(".tarkki2").html("");
            $("#1_numero_3").removeClass("vaarin");
            $("#1_numero_3").removeClass("oikein");

            $(".tarkki3").html("");
            $("#1_numero_4").removeClass("vaarin");
            $("#1_numero_4").removeClass("oikein");
            return false;
        } else {
            $(".next").show();
            $(".vastaa").hide();
        }
        $("#tarkista").attr("disabled", true);
        $(".texts").show();
        let value = Number($("input[name=teht_1]:checked").val());

        let vastaus1 = Number($("#correct1").val());
        let luku1 = Number($("#1_numero_1").html());
        let va1 = $("input[id=correct1]").val();

        let vastaus2 = Number($("#correct2").val());
        let luku2 = Number($("#1_numero_2").html());
        let va2 = $("input[id=correct2]").val();

        let vastaus3 = Number($("#correct3").val());
        let luku3 = Number($("#1_numero_3").html());
        let va3 = $("input[id=correct3]").val();

        let vastaus4 = Number($("#correct4").val());
        let luku4 = Number($("#1_numero_4").html());
        let va4 = $("input[id=correct4]").val();

        // tarkistetaan jos vastaus on oikein tai väärin + piilotetaan vastaa kaikkiin teksti
        if (value === 0) {
            if (vastaus1 !== Math.round(luku1)) {
                $(".tarkki").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_1").addClass("vaarin");
                $(".vastaa").hide();
            } else if (va1 === "") {
                $("#tarkista").attr("disabled", false);
                $(".vastaa").show();
                $(".tarkki").html("");
                $("#1_numero_1").removeClass("vaarin");
                $("#1_numero_1").removeClass("oikein");
            } else {
                $(".tarkki").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_1").addClass("oikein");
                pisteet++;
                $(".vastaa").hide();
            }

            if (vastaus2 !== Math.round(luku2)) {
                $(".tarkki1").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_2").addClass("vaarin");
                $(".vastaa").hide();
            } else if (va2 === "") {
                $("#tarkista").attr("disabled", false);
                $(".vastaa").show();
                $(".tarkki1").html("");
                $("#1_numero_2").removeClass("vaarin");
                $("#1_numero_2").removeClass("oikein");
            } else {
                $(".tarkki1").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_2").addClass("oikein");
                pisteet++;
                $(".vastaa").hide();
            }

            if (vastaus3 !== Math.round(luku3)) {
                $(".tarkki2").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_3").addClass("vaarin");
                $(".vastaa").hide();
            } else if (va3 === "") {
                $("#tarkista").attr("disabled", false);
                $(".vastaa").show();
                $(".tarkki2").html("");
                $("#1_numero_3").removeClass("vaarin");
                $("#1_numero_3").removeClass("oikein");
            } else {
                $(".tarkki2").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_3").addClass("oikein");
                pisteet++;
                $(".vastaa").hide();
            }

            if (vastaus4 !== Math.round(luku4)) {
                $(".tarkki3").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_4").addClass("vaarin");
                $(".vastaa").hide();
            } else if (va4 === "") {
                $("#tarkista").attr("disabled", false);
                $(".vastaa").show();
                $(".tarkki3").html("");
                $("#1_numero_4").removeClass("vaarin");
                $("#1_numero_4").removeClass("oikein");
            } else {
                $(".tarkki3").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_4").addClass("oikein");
                pisteet++;
                $(".vastaa").hide();
            }
        }

        let num = Number(luku1.toFixed(1));
        let num1 = Number(luku2.toFixed(1));
        let num2 = Number(luku3.toFixed(1));
        let num3 = Number(luku4.toFixed(1));
        if (value === 1) {
            if (vastaus1 !== num) {
                $(".tarkki").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_1").addClass("vaarin");
            } else {
                $(".tarkki").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_1").addClass("oikein");
                pisteet++;
            }
            
            if (vastaus2 !== num1) {
                $(".tarkki1").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_2").addClass("vaarin");
            } else {
                $(".tarkki1").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_2").addClass("oikein");
                pisteet++;
            }
            
            if (vastaus3 !== num2) {
                $(".tarkki2").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_3").addClass("vaarin");
            } else {
                $(".tarkki2").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_3").addClass("oikein");
                pisteet++;
            }
            
            if (vastaus4 !== num3) {
                $(".tarkki3").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_4").addClass("vaarin");
            } else {
                $(".tarkki3").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_4").addClass("oikein");
                pisteet++;
            }
        }

        let num0 = Number(luku1.toFixed(2));
        let num9 = Number(luku2.toFixed(2));
        let num8 = Number(luku3.toFixed(2));
        let num7 = Number(luku4.toFixed(2));
        if (value === 2) {
            if (vastaus1 !== num0) {
                $(".tarkki").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_1").addClass("vaarin");
            } else {
                $(".tarkki").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_1").addClass("oikein");
                pisteet++;
            }
            
            if (vastaus2 !== num9) {
                $(".tarkki1").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_2").addClass("vaarin");
            } else {
                $(".tarkki1").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_2").addClass("oikein");
                pisteet++;
            }

            if (vastaus3 !== num8) {
                $(".tarkki2").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_3").addClass("vaarin");
            } else {
                $(".tarkki2").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_3").addClass("oikein");
                pisteet++;
            }

            if (vastaus4 !== num7) {
                $(".tarkki3").html("Voi rähmä <i class='fa fa-frown-o' aria-hidden='true'></i>");
                $("#1_numero_4").addClass("vaarin");
            } else {
                $(".tarkki3").html("Upeaa, sait <i class='fa fa-puzzle-piece' aria-hidden='true'></i>");
                $("#1_numero_4").addClass("oikein");
                pisteet++;
            }
        }
    });

});