$(document).ready(function () {
    // Satunnaislukugeneraattori
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    // Visan muuttujia
    kysymykset3 = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10"];
    let pokaali = '<i class="fa fa-trophy fa-5x text-warning" aria-hidden="true"></i>';
    let suru = '<i class="fa fa-frown-o fa-5x" aria-hidden="true"></i>';
    oikeat_vastaukset = 0;
    tahti = 0;
    let yritys = 0;

    // Visan Tarkista-painikkeen toiminto
    $(".tarkista").click(function () {

        if ($('input[type=radio]').is(":checked") === true) {

            let vastaus = Number($(".vastaa:checked").val());
            let kysymys = $(".vastaa:checked").attr("name");
            $("#" + kysymys + "_yrita").hide();

            if (vastaus === 0) {

                if (yritys < 1) {
                    yritys++;
                    $("#" + kysymys + "_yrita").show();
                } else {
                    $($(".vastaa:checked")).next().addClass("vastaus_teksti");
                    $($(".vastaa:checked")).parent().addClass("vaarin");
                    $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
                    $("[name=" + kysymys + "]").prop("disabled", true);
                    $("#" + kysymys + "_selitys").show();
                    $("#" + kysymys + "_next").show();
                    $(this).hide();
                    $($(".vastaa:checked")).prop("checked", false);
                    $("#tahti" + tahti).html("<i class='fa fa-star-o fa-2x text-danger' aria-hidden='true'></i>")
                    yritys = 0;
                }

            }
            if (vastaus === 1) {
                $($(".vastaa:checked")).next().addClass("vastaus_teksti");
                $($(".vastaa:checked")).parent().addClass("oikein");
                $("#tahti" + tahti).html("<i class='fa fa-star fa-2x fa-spin text-warning' aria-hidden='true'></i>");
                oikeat_vastaukset++;
                $("#" + kysymys + "_selitys").show();
                $("[name=" + kysymys + "]").prop("disabled", true);
                $("#" + kysymys + "_next").show();
                $(this).hide();
                $($(".vastaa:checked")).prop("checked", false);
                yritys = 0;
            }

        } else {
            $("#vastaus_puuttuu").modal("show");
        }
    });

    // Visan Aloita- ja Seuraava-painikkeiden toiminta
    $(".seuraava").click(function () {
        tahti++;
        if (kysymykset3.length > 0) {
            let numero = getRndInteger(0, kysymykset3.length - 1);
            let esille = kysymykset3[numero];
            $(esille).show();
            kysymykset3.splice(numero, 1);
            $(this).parent().hide();
        } else {
            $(this).parent().hide();
            $("#lopputulos").show();

            if (oikeat_vastaukset === 10) {
                $("#titteli").html("Olet varmaankin avaruusolio! Sait täydet 10 pistettä.");
                $("#pokaali").html(pokaali + " " + pokaali + " " + pokaali);
            } else if (oikeat_vastaukset > 6) {
                $("#titteli").html("Olet mestariastronautti! Sait " + oikeat_vastaukset + " pistettä.");
                $("#pokaali").html(pokaali + " " + pokaali);
            } else if (oikeat_vastaukset > 2) {
                $("#titteli").html("Olet avaruustietäjä! Sait " + oikeat_vastaukset + " pistettä.");
                $("#pokaali").html(pokaali);
            } else {
                $("#titteli").html("Olet avaruusn00b. Yritä uudelleen! Sait " + oikeat_vastaukset + " pistettä.");
                $("#pokaali").html(suru);
            }
        }

    });

//    matikkatehtävien koodi


    let numerot = 0;
    let kertojat = 0;
    let tehtavia = 0;
    let i = 0;
    let vaikeus = 0;
    let kertapisteet = true;
    let matikkapisteet = 0;
    let kertojalista = [0];
    let numerolista = [0];
    let tyyppi = 0;

    // Funktio asettaa numeroarrayt uusiksi edellisen arvonnan jälkeen vaikeusasteeen mukaan.
    function kertojienListaus() {
        numerot = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        switch (vaikeus) {
            case 1:
                kertojat = [0, 1, 2, 5, 10];
                break;
            case 2:
                kertojat = [0, 1, 2, 3, 4, 5, 6, 10];
                break;
            case 3:
                kertojat = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
                break;
        }
    }

    /*
     * Funktio, joka hakee satunnaislukugeneraattorilta indeksinumeron, jonka perusteella
     * hakee numerolistasta indeksin mukaisen numeron ja asettaa sen oikeaan paikkaan
     * sivulla ja asettaa indeksiin arvoksi 0. Mikäli haetun indeksin arvo on 0, 
     * hakee uuden luvun kunnes löytää indeksin, jossa on muu kuin 0. 
     */
    function arvoNumerot() {
        for (let i = 1; i < 5; i++) {
            while (true) {
                let numero = getRndInteger(1, numerot.length - 1);
                if (numerot[numero] === 0) {
                    continue;
                } else {
                    numerolista.push(numerot[numero]);
                    numerot[numero] = 0;
                    break;
                }
            }
        }

        for (let i = 1; i < 5; i++) {
            while (true) {
                let numero = getRndInteger(1, kertojat.length - 1);
                if (kertojat[numero] === 0) {
                    continue;
                } else {
                    kertojalista.push(kertojat[numero]);
                    kertojat[numero] = 0;
                    break;
                }
            }
        }

        for (let i = 1; i < 5; i++) {
            if (tyyppi === 1) {
                $("#kertot" + i).html(numerolista[i]);
            } else {
                $("#jaettava" + i).html(numerolista[i] * kertojalista[i]);
            }
        }

        if (tyyppi === 1) {
            for (let i = 1; i < 5; i++) {
                if (i === 2 || i === 4) {
                    $("#vastaus" + i).html(numerolista[i] * kertojalista[i]);
                } else {
                    $("#kertoja" + i).html(kertojalista[i]);
                }
            }
        } else {
            for (let i = 1; i < 5; i++) {
                $("#jakaja" + i).html(kertojalista[i]);
            }
        }
    }

    /*
     * Funktio tarkistaa vastauksen sen mukaan, onko kyseessä kerto- vai jakolasku. 
     * Muuttuja "tyyppi" on globaali, jotta sitä voi käyttää useammassa click-eventissä
     * ja se kertoo laskun tyypin (1=kerto, 2=jako). 
     */

    function tarkistaVastaus() {
        if (tyyppi === 1) {
            for (let i = 1; i < 5; i++) {
                if (i === 2 || i === 4) {

                    if (kertojalista[i] === Number($("#kertoja" + i).val())) {
                        $("#kertoja" + i).addClass("oikein");
                        matikkapisteet++;
                    } else {
                        $("#kertoja" + i).addClass("vaarin");
                    }

                } else {

                    if (Number($("#vastaus" + i).val()) === (kertojalista[i] * numerolista[i])) {
                        $("#vastaus" + i).addClass("oikein");
                        matikkapisteet++;
                    } else {
                        $("#vastaus" + i).addClass("vaarin");
                    }
                }
            }
        } else {
            for (let i = 1; i < 5; i++) {
                if (Number($("#jakovast" + i).val()) === numerolista[i]) {
                    $("#jakovast" + i).addClass("oikein");
                    matikkapisteet++;
                } else {
                    $("#jakovast" + i).addClass("vaarin");
                }
            }
        }

    }

    /*
     * Funktio asettaa toiminnan vilkkuvalle peukalolle, jonka saa kun vastaa jokaiseen
     * sivulla olevaan laskuun oikein.
     */
    function peukalo() {
        let peukalo;
        peukalo = $("#kertapist" + tyyppi);
        peukalo.html('<i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i>');
        setTimeout(function () {
            peukalo.html('<i class="fa fa-thumbs-up fa-2x" aria-hidden="true"></i>');
        }, 250);
    }


    // Aloita-painikkeen toiminto

    $(".arvonum").click(function () {
        if (($("[name=tyyppi]").is(':checked')) === false) {
            $("#valitse_lasku").modal("show");

        } else {
            i++;
            vaikeus = Number($("#vaikeus").val());
            tehtavia = Number($("#maara").val());
            tyyppi = Number($("[name=tyyppi]:checked").val());

            kertojienListaus();

            $(".matikkaAlku").hide();
            $(".laskutark").show();
            $(".matikka" + tyyppi).show();

            arvoNumerot();
        }

    });

    //Tarkista-painikkeen toiminto
    $(".laskutark").click(function () {

        /* Tarkistaa, että jokaiseen kysymykseen on vastattu numeroilla ennen kuin tarkistaa
         * vastaukset. Jos joku kentistä on tyhjä tai sisältää kirjaimia, pyytää tarkistamaan
         * kenttien sisällön.*/

        let kentat = true;
        $(".annettu" + tyyppi).each(function () {

            if (!($.isNumeric($(this).val()))) {
                $("#vain_numeroita").modal("show");
                kentat = false;
            }


        });

        if (kentat === true) {
            tarkistaVastaus();

            $(".annettu" + tyyppi).each(function () {
                if (!($(this).hasClass("oikein"))) {
                    kertapisteet = false;
                }
            });

            if (kertapisteet === true) {
                $("#kertapist" + tyyppi).show();
                peukalo();
                setInterval(peukalo, 500);
            }

            $(".laskutark").hide();
            $(".arvoUudet").show();
            $("#pisteitaNyt" + tyyppi).html(matikkapisteet);
        }
    });

    /*Seuraava-painikkeen toiminto*/
    $(".arvoUudet").click(function () {
        $("#kertapist" + tyyppi).hide();

        if (i < tehtavia) {
            kertojalista = [0];
            numerolista = [0];
            kertapisteet = true;
            i++;
            $(".annettu" + tyyppi).val("");
            $(".annettu" + tyyppi).removeClass("oikein");
            $(".annettu" + tyyppi).removeClass("vaarin");
            $(this).hide();
            $(".laskutark").show();

            kertojienListaus();
            arvoNumerot();

        } else {
            $(this).hide();
            $(".matikka" + tyyppi).hide();
            $(".matikkaloppu").show();
            $("#pisteitaLop").html(matikkapisteet);
            $("#pisteitaMaks").html(tehtavia * 4);

            let lamppu = "<i class='fa fa-lightbulb-o fa-5x text-warning' aria-hidden='true'></i>";
            let peukku = '<i class="fa fa-thumbs-up fa-4x" aria-hidden="true"></i>';
            if ((matikkapisteet / (tehtavia * 4)) > 0.8) {
                $("#teksti").html("Olet matikkanero!");
                $("#lamppu").html(lamppu + "  " + lamppu + "  " + lamppu);
            } else if ((matikkapisteet / (tehtavia * 4)) > 0.5) {
                $("#teksti").html("Olet melko taitava!");
                $("#lamppu").html(lamppu + "  " + lamppu);
            } else if ((matikkapisteet / (tehtavia * 4)) > 0.2) {
                $("#teksti").html("Osaat vähän.");
                $("#lamppu").html(lamppu);
            } else if ((matikkapisteet / (tehtavia * 4)) >= 0) {
                $("#teksti").html("Harjoittele lisää, opit kyllä!");
                $("#lamppu").html(peukku);
            }
        }

    });
});
