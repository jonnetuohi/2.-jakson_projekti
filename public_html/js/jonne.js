$(document).ready(function () {
    $(".answer").click(function () {
        // Tarkistetaan mikä vastausvaihtoehto on valittu.
        let valittu = Number($(this).val());
        // Sidotaan valinta valittuun kysymykseen.
        let kysymys = $(this).attr("name");
        $("[name=" + kysymys + "]").prop("disabled", true);
        // Näytetään selitys.
        $("#" + kysymys + "_selitys").show();
        // Tarkistetaan onko valinta oikein ja jos on niin lisätään vilkkuva vihreä teksti ja lasketaan +1 piste.
        if (valittu === 1) {
            $(this).addClass("vastaus_teksti");
            $(this).parent().children().next().addClass("vilkku");
            $("#keratytpisteet").append("<i class='fa fa-check fa-3x vihrea' aria-hidden='true'></i>");
            oikein_vastattu++;
            $(".seuraava3").removeClass('hided');
            // Jos vastaus on väärin lisätään valitulle vastaukselle vilkkuva punainen teksti ja oikealle vastaukselle vihreä vilkkuva teksti.
        } else {
            $(this).next().addClass("vilkku2");
            // haetaan oikea vastaus ja lihavoidaan seuraavan elementin teksti
            $("[name=" + kysymys + "][value=1]").next().addClass("vilkku");
            $("#keratytpisteet").append("<i class='fa fa-ban fa-3x text-danger' aria-hidden='true'></i>");
            $(".seuraava3").removeClass('hided');
        }
    });
    // Tehdään kaikista kysymyksistä lista, josta voidaan täten ottaa kysymyksiä satunnaisessa järjestyksessä.
    kysymykset3 = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10"];

    /*
     * Arvotaan kysymykset visaan satunnaisessa järjestyksessä ja poistetaan arvottu kysymys vastauksen jälkeen.
     * Pisteet ja titteli määräytyy saatujen pisteiden perusteella ja tulostetaan titteli sen jälkeen kun kaikki kysymykset on käyty läpi.
     */
    $(".seuraava3").click(function () {
        $(".seuraava3").addClass('hided');
        if (kysymykset3.length > 0) {
            let kysymys = getRndInteger(0, kysymykset3.length - 1);
            let esille = kysymykset3[kysymys];
            $(esille).show();
            kysymykset3.splice(kysymys, 1);
            // painikkeen vanhempi piiloitetaan!!!! TÄMÄ ON TÄRKEÄ !!
            $(this).parent().hide();
        } else if (oikein_vastattu < 4) {
            $(this).parent().hide();
            $("#tulos").show();
            $("#pisteet").html(oikein_vastattu + "/10 " + "Olet perustietäjä!");
        } else if (oikein_vastattu < 8) {
            $(this).parent().hide();
            $("#tulos").show();
            $("#pisteet").html(oikein_vastattu + "/10 " + "Todella hyvä suoritus!");
        } else {
            $(this).parent().hide();
            $("#tulos").show();
            $("#pisteet").html(oikein_vastattu + "/10 " + "Loistavaa, olet supertietäjä!");
        }
    });
    $(".alusta").click(function () {
        location.reload();
    });

    /*
     * lasketaan visan oikeat vastaukset tähän.
     */
    oikein_vastattu = 0;


    /*
     * Tällä functiolla arvotaan satunnaisia numeroita siltä väliltä mitkä siihen asetetaan.
     */
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    /*
     * Matikkapelin yhteenlasku nappi aloittaa nämä toiminnot.
     * piiloitetaan alkuvalikko.
     * poistetaan yhteenlasku div:in class hided, jotta saadaan tehtävät näkyviin.
     * arvotaan tehtäville satunnaiset numerot.
     */

    $("#yhteen").click(function () {
        $(".valikko").hide();
        $(".yhteenlasku").removeClass('hided');
        arvottu1 = (getRndInteger(20, 200));
        $("#numero1").html(arvottu1);
        $("#vaadittu1").html(arvottu1);

        arvottu2 = (getRndInteger(20, 200));
        $("#numero2").html(arvottu2);
        $("#vaadittu2").html(arvottu2);

        arvottu3 = (getRndInteger(20, 200));
        $("#numero3").html(arvottu3);
        $("#vaadittu3").html(arvottu3);
    });

    /*
     * Tarkista napin toiminnot alkavat tässä.
     * Tarkistetaan onko vastauksiin jätetty vastaamatta tai onko syötetty muutakuin numeroita.
     * Jos on jotakin huomautettavaa vastauksissa, tai ei ole toimittu haluamallani tavalla annetaan käyttäjälle ohjeet tilanteen korjaamiseksi.
     */
    $("#tarkista").click(function () {
        if ($("#nro1").val() === "" || $("#nro2").val() === "" || $("#nro3").val() === "" || $("#nro4").val() === "" || $("#nro5").val() === "" || $("#nro6").val() === "" || $("#nro7").val() === "" || $("#nro8").val() === "" || $("#nro9").val() === "") {
            $("#vastaa").modal('toggle');
            return;
        }
        if (isNaN($("#nro1").val())) {
            $("#numeroilla").modal('toggle');
            $("#nro1").val("");
            return;
        }
        if (isNaN($("#nro2").val())) {
            $("#numeroilla").modal('toggle');
            $("#nro2").val("");
            return;
        }
        if (isNaN($("#nro3").val())) {
            $("#numeroilla").modal('toggle');
            $("#nro3").val("");
            return;
        }
        if (isNaN($("#nro4").val())) {
            $("#numeroilla").modal('toggle');
            $("#nro4").val("");
            return;
        }
        if (isNaN($("#nro5").val())) {
            $("#numeroilla").modal('toggle');
            $("#nro5").val("");
            return;
        }
        if (isNaN($("#nro6").val())) {
            $("#numeroilla").modal('toggle');
            $("#nro6").val("");
            return;
        }
        if (isNaN($("#nro7").val())) {
            $("#numeroilla").modal('toggle');
            $("#nro7").val("");
            return;
        }
        if (isNaN($("#nro8").val())) {
            $("#numeroilla").modal('toggle');
            $("#nro8").val("");
            return;
        }
        if (isNaN($("#nro9").val())) {
            $("#numeroilla").modal('toggle');
            $("#nro9").val("");
            return;
        }

        /*
         * Lasketaan mikä vastauksen tulisi olla ja verrataan vastausta käyttäjän antamaan tulokseen.
         * Tässä myös lasketaan pisteet.
         */

        let tarvittu1 = Number($("#nro1").val()) + Number($("#nro2").val()) + Number($("#nro3").val());
        if (tarvittu1 === arvottu1) {
            $("#tulos1").html("<i class='fa fa-check vihrea' aria-hidden='true'></i>");
            pisteet3++;
        } else {
            $("#tulos1").html("<i class='fa fa-ban text-danger' aria-hidden='true'></i>");
        }

        let tarvittu2 = Number($("#nro4").val()) + Number($("#nro5").val()) + Number($("#nro6").val());
        if (tarvittu2 === arvottu2) {
            $("#tulos2").html("<i class='fa fa-check vihrea' aria-hidden='true'></i>");
            pisteet3++;
        } else {
            $("#tulos2").html("<i class='fa fa-ban text-danger' aria-hidden='true'></i>");
        }

        let tarvittu3 = Number($("#nro7").val()) + Number($("#nro8").val()) + Number($("#nro9").val());
        if (tarvittu3 === arvottu3) {
            $("#tulos3").html("<i class='fa fa-check vihrea' aria-hidden='true'></i>");
            pisteet3++;
        } else {
            $("#tulos3").html("<i class='fa fa-ban text-danger' aria-hidden='true'></i>");
        }
        /*
         * Tulostetaan pisteet.
         */
        $("#pisteetyhteen").html(pisteet3);
        $("#tarkista").hide();
        $("#uudet2").prop("disabled", false);
    });

    /*
     * Matikkapelin vähennyslasku nappi aloittaa nämä toiminnot.
     * piiloitetaan alkuvalikko.
     * poistetaan vahennyslasku div:in class hided, jotta saadaan tehtävät näkyviin.
     * arvotaan tehtäville satunnaiset numerot.
     */

    $("#vahennys").click(function () {
        $(".valikko").hide();
        $(".vahennyslasku").removeClass('hided');
        aloitusnumero1 = (getRndInteger(35, 50));
        $("#alkunumero1").html(aloitusnumero1);
        vahennys1 = (getRndInteger(10, 25));
        $("#vahennettava1").html(vahennys1);

        aloitusnumero2 = (getRndInteger(10, 20));
        $("#alkunumero2").html(aloitusnumero2);
        vahennys3 = (getRndInteger(1, 9));
        $("#vahennettava3").html(vahennys3);

        aloitusnumero3 = (getRndInteger(50, 75));
        $("#alkunumero3").html(aloitusnumero3);
        vahennys5 = (getRndInteger(10, 35));
        $("#vahennettava5").html(vahennys5);

        aloitusnumero4 = (getRndInteger(15, 20));
        $("#alkunumero4").html(aloitusnumero4);
        vahennys7 = (getRndInteger(1, 14));
        $("#vahennettava7").html(vahennys7);

        aloitusnumero5 = (getRndInteger(50, 100));
        $("#alkunumero5").html(aloitusnumero5);
        vahennys9 = (getRndInteger(10, 20));
        $("#vahennettava9").html(vahennys9);

    });
    /*
     * Tarkista napin toiminnot alkavat tässä.
     * Tarkistetaan onko vastauksiin jätetty vastaamatta tai onko syötetty muutakuin numeroita.
     * Jos on jotakin huomautettavaa vastauksissa, tai ei ole toimittu haluamallani tavalla annetaan käyttäjälle ohjeet tilanteen korjaamiseksi.
     */

    $("#tarkista2").click(function () {
        if ($("#vastaus1").val() === "" || $("#vastaus2").val() === "" || $("#vastaus3").val() === "" || $("#vastaus4").val() === "" || $("#vastaus5").val() === "") {
            $("#vastaa").modal('toggle');
            return;
        }
        if (isNaN($("#vastaus1").val())) {
            $("#numeroilla").modal('toggle');
            $("#vastaus1").val("");
            return;
        }
        if (isNaN($("#vastaus2").val())) {
            $("#numeroilla").modal('toggle');
            $("#vastaus2").val("");
            return;
        }
        if (isNaN($("#vastaus3").val())) {
            $("#numeroilla").modal('toggle');
            $("#vastaus3").val("");
            return;
        }
        if (isNaN($("#vastaus4").val())) {
            $("#numeroilla").modal('toggle');
            $("#vastaus4").val("");
            return;
        }
        if (isNaN($("#vastaus5").val())) {
            $("#numeroilla").modal('toggle');
            $("#vastaus5").val("");
            return;
        }

        /*
         * Lasketaan mikä vastauksen tulisi olla ja verrataan vastausta käyttäjän antamaan tulokseen.
         * Tässä myös lasketaan pisteet.
         */

        let tarvittu4 = aloitusnumero1 - vahennys1;
        if (tarvittu4 === Number($("#vastaus1").val())) {
            $("#tulos4").html("<i class='fa fa-check vihrea' aria-hidden='true'></i>" + "Hienoa!");
            $("#vastaus1").prop('disabled', true);
            pisteet2++;
        } else if (tarvittu4 !== Number($("#vastaus1").val())) {
            $("#tulos4").html("<i class='fa fa-ban text-danger' aria-hidden='true'></i>" + " Oikea vastaus on " + tarvittu4);
            $("#vastaus1").prop('disabled', true);
        }
        let tarvittu5 = aloitusnumero2 - vahennys3;
        if (tarvittu5 === Number($("#vastaus2").val())) {
            $("#tulos5").html("<i class='fa fa-check vihrea' aria-hidden='true'></i>" + "Hienoa!");
            $("#vastaus2").prop('disabled', true);
            pisteet2++;
        } else if (tarvittu5 !== Number($("#vastaus2").val())) {
            $("#tulos5").html("<i class='fa fa-ban text-danger' aria-hidden='true'></i>" + " Oikea vastaus on " + tarvittu5);
            $("#vastaus2").prop('disabled', true);
        }
        let tarvittu6 = aloitusnumero3 - vahennys5;
        if (tarvittu6 === Number($("#vastaus3").val())) {
            $("#tulos6").html("<i class='fa fa-check vihrea' aria-hidden='true'></i>" + "Hienoa!");
            $("#vastaus3").prop('disabled', true);
            pisteet2++;
        } else if (tarvittu6 !== Number($("#vastaus3").val())) {
            $("#tulos6").html("<i class='fa fa-ban text-danger' aria-hidden='true'></i>" + " Oikea vastaus on " + tarvittu6);
            $("#vastaus3").prop('disabled', true);
        }
        let tarvittu7 = aloitusnumero4 - vahennys7;
        if (tarvittu7 === Number($("#vastaus4").val())) {
            $("#tulos7").html("<i class='fa fa-check vihrea' aria-hidden='true'></i>" + "Hienoa!");
            $("#vastaus4").prop('disabled', true);
            pisteet2++;
        } else if (tarvittu7 !== Number($("#vastaus4").val())) {
            $("#tulos7").html("<i class='fa fa-ban text-danger' aria-hidden='true'></i>" + " Oikea vastaus on " + tarvittu7);
            $("#vastaus4").prop('disabled', true);
        }
        let tarvittu8 = aloitusnumero5 - vahennys9;
        if (tarvittu8 === Number($("#vastaus5").val())) {
            $("#tulos8").html("<i class='fa fa-check vihrea' aria-hidden='true'></i>" + "Hienoa!");
            $("#vastaus5").prop('disabled', true);
            pisteet2++;
        } else if (tarvittu8 !== Number($("#vastaus5").val())) {
            $("#tulos8").html("<i class='fa fa-ban text-danger' aria-hidden='true'></i>" + " Oikea vastaus on " + tarvittu8);
            $("#vastaus5").prop('disabled', true);
        }
        $("#pisteetvahennyksesta").html(pisteet2);
        $("#tarkista2").hide();
        $("#uudet").prop("disabled", false);
    });
    /*
     * Arvotaan uudet numerot vähennyslasku tehtäviin. painikkeelle on asetettu tieto, että sitä voi painaa ainoastaan kerran.
     */

    $("#uudet").one('click', function () {
        $("#vastaus1").prop('disabled', false);
        $("#vastaus2").prop('disabled', false);
        $("#vastaus3").prop('disabled', false);
        $("#vastaus4").prop('disabled', false);
        $("#vastaus5").prop('disabled', false);
        $("#tarkista2").show();

        aloitusnumero1 = (getRndInteger(35, 50));
        $("#alkunumero1").html(aloitusnumero1);
        vahennys1 = (getRndInteger(10, 25));
        $("#vahennettava1").html(vahennys1);

        aloitusnumero2 = (getRndInteger(10, 20));
        $("#alkunumero2").html(aloitusnumero2);
        vahennys3 = (getRndInteger(1, 9));
        $("#vahennettava3").html(vahennys3);

        aloitusnumero3 = (getRndInteger(50, 75));
        $("#alkunumero3").html(aloitusnumero3);
        vahennys5 = (getRndInteger(10, 35));
        $("#vahennettava5").html(vahennys5);

        aloitusnumero4 = (getRndInteger(15, 20));
        $("#alkunumero4").html(aloitusnumero4);
        vahennys7 = (getRndInteger(1, 14));
        $("#vahennettava7").html(vahennys7);

        aloitusnumero5 = (getRndInteger(50, 100));
        $("#alkunumero5").html(aloitusnumero5);
        vahennys9 = (getRndInteger(10, 20));
        $("#vahennettava9").html(vahennys9);
        $("#tulos4").html("");
        $("#tulos5").html("");
        $("#tulos6").html("");
        $("#tulos7").html("");
        $("#tulos8").html("");
        $("#vastaus1").val("");
        $("#vastaus2").val("");
        $("#vastaus3").val("");
        $("#vastaus4").val("");
        $("#vastaus5").val("");
        $("#uudet").hide();
        $("#kokonaistulos").prop("disabled", false);
    });
    /*
     * Arvotaan uudet numerot yhteenlasku tehtäviin. painikkeelle on asetettu tieto, että sitä voi painaa ainoastaan kerran.
     */

    $("#uudet2").one('click', function () {
        $("#tarkista").show();
        $(".valikko").hide();
        $(".yhteenlasku").removeClass('hided');
        arvottu1 = (getRndInteger(20, 200));
        $("#numero1").html(arvottu1);
        $("#vaadittu1").html(arvottu1);

        arvottu2 = (getRndInteger(20, 200));
        $("#numero2").html(arvottu2);
        $("#vaadittu2").html(arvottu2);

        arvottu3 = (getRndInteger(20, 200));
        $("#numero3").html(arvottu3);
        $("#vaadittu3").html(arvottu3);
        $("#tulos1").html("");
        $("#tulos2").html("");
        $("#tulos3").html("");
        $("#nro1").val("");
        $("#nro3").val("");
        $("#nro4").val("");
        $("#nro5").val("");
        $("#nro6").val("");
        $("#nro7").val("");
        $("#nro9").val("");
        $("#lopetus").prop("disabled", false);
        $("#uudet2").hide();
    });
    pisteet2 = 0;
    pisteet3 = 0;

    /*
     * Vertaillaan kuinka monta pistettä käyttäjä on saanut ja palkitaan hänet sen mukaan.
     */

    $("#kokonaistulos").click(function () {
        $(".vahennyslasku").hide();
        $("#valmis").show();

        if (pisteet2 < 6) {
            $("#pisteet2").html(pisteet2 + "/10 " + "Vielä on parannettavaa!");
        }
        if (pisteet2 > 6) {
            $("#pisteet2").html(pisteet2 + "/10 " + "Todella hyvä suoritus!");
        }
    });
    $("#lopetus").click(function () {
        $(".yhteenlasku").hide();
        $("#valmis1").show();

        if (pisteet3 < 3) {
            $("#pisteet3").html(pisteet3 + "/6 " + "Vielä on parannettavaa!");
        }
        if (pisteet3 > 3) {
            $("#pisteet3").html(pisteet3 + "/6 " + "Todella hyvä suoritus!");
        }
    });
});




