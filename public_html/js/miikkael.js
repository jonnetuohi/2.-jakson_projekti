

let palkinnot = ['<i class="fa fa-2x fa-bolt keltainen" aria-hidden="true"></i>',
    '<i class="fa fa-2x fa-times punainen" aria-hidden="true"></i>']
let oikeat = [];
let väärät = [];



$(document).ready(function () {
    // arpoo numeron min ja max väliltä
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    //vastauksen tarkistus
    $(".vastaus").click(function () {

        let vastaus = Number($(this).val())
        let kysymys = $(this).attr("name")

        //jos vastaus oikein. salamamerkki tulostetaan, lasketaan oikeat vastaukset
        if (vastaus === 1) {
            $("button.seuraava3").prop("disabled", false);
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("oikein");
            $("#" + kysymys + "_selitys").fadeIn(300);
            $("#palkinto").append(`<li> ${palkinnot[0]}</li>`);
            $("[name=" + kysymys + "]").prop("disabled", true);
            oikeat.push('oikein')
            $("button.seuraava3").html("Seuraava")
            väärät.splice(0, 2)
        }


        else {

            väärät.push('väärin')

            for (let i = 0; i < väärät.length; i++) {
                //jos vastaa kerran väärin. saa yrittää uudelleen
                if (väärät.length === 1) {
                    $("#uudelleen").fadeIn(1500).fadeOut(1000)
                    $("button.seuraava3").prop("disabled", true);
                    $("button.seuraava3").html("Seuraava")

                    // väärä vastaus merkitään jos vastaa toisen kerran väärin.
                } else if (väärät.length === 2) {
                    $("button.seuraava3").prop("disabled", false);
                    $("[name=" + kysymys + "]").prop("disabled", true);
                    $("button.seuraava3").html("Seuraava")
                    väärät.splice(0, 2)
                    $("#palkinto").append(`<li> ${palkinnot[1]}</li>`);

                }

            }

        }

    });





    // Kysymykset
    kysymykset3 = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5",
        "#kysymys6", "#kysymys7", "#kysymys8", "#kysymys9", "#kysymys10",];
    /**
     * tuodaan kysymykset esille yksi kerrallaan sekalaisessa järjestyksessä
     */
    $(".seuraava3").click(function () {
        $(".salama p ").hide()
        if (kysymykset3.length > 0) {
            let kysymys = getRndInteger(0, kysymykset3.length - 1);
            let esille = kysymykset3[kysymys];
            $(esille).fadeIn(300);
            kysymykset3.splice(kysymys, 1)
            $(this).parent().hide();
        } else {
            $("button.lataa").show();
            $(this).parent().hide()

            // kerrotaan käyttäjälle kuinka monta meni oikein. Lisäksi palkinto jos yli 5 oikein.
            
            switch (oikeat.length) {
                case 0:
                    $("#loppupalkinto").html("Et taida tietää Harry Potterista? Voit kokeilla uudestaan.")
                    break;
                case 1:
                    $("#loppupalkinto").html("Sait yhden oikein. Voit vaikka kokeilla uudestaan.")
                    break;
                case 2:
                    $("#loppupalkinto").html("Sait kaksi oikein. Voit vaikka kokeilla uudestaan.")
                    break;
                case 3:
                    $("#loppupalkinto").html("Hei! sait 3 oikein. Hienoa!")
                    break;
                case 4:
                    $("#loppupalkinto").html("Sait 4 oikein!")
                    break;
                case 5:
                    $("#loppupalkinto").html("Loistavaa! 5 oikein!")
                    break;
                case 6:
                    $("#loppupalkinto").html("Sait 6 oikein. Hienoa!")
                    $(".pokaali").parent().show().addClass("pronssi");
                    break;
                case 7:
                    $("#loppupalkinto").html("Sait 7 oikein. Loistavaa! Tiedät paljon Harry Potterista")
                    $(".pokaali").parent().show().addClass("pronssi");
                    break;
                case 8:
                    $("#loppupalkinto").html("Mahtavaa! Sait 8 oikein. Olet Pottertietäjä")
                    $(".pokaali").parent().show().addClass("hopea");
                    break;
                case 9:
                    $("#loppupalkinto").html("Hienoa! 9 oikein ja vain 1 väärin. Loistavaa!")
                    $(".pokaali").parent().show().addClass("hopea");
                    break;
                case 10:
                    $("#loppupalkinto").html("Loistavaa!! Kaikki oikein! Olet varsinainen Hermione Granger. Tiedät kaiken!")
                    $(".pokaali").parent().show().addClass("kulta");
                    break;
                default:

            }

        }


    });

    //  *******Matikkapelin koodit********//

    /** Arpoo luvun Min-Max väliltä
     * 
     * @param {Number} min 
     * @param {Number} max 
     * @return {Number} luku
     */
    function ArvoLuku(min, max) {
        luku = getRndInteger(min, max)
        return luku
    }
    /**
     * Lukee käyttäjän valitseman kertojan
     * @return {number}  Palauttaa kertojan arvon.
     */
    function HaeKertoja() {
        let arvo = ($('option[name=kerto]:checked').val())

        return arvo;

    }

    /**
     * Lukee käyttäjän valitseman laskualueen
     * @return {number} arvotun luvun valitulta väliltä.
     * 
     */
    function LaskuAlue() {
        let alue = ($('option[name=alue]:checked').val())
        if (alue === "14") {  // 1-10 alue
            let helppo = ArvoLuku(1, 10)
            return helppo;
        } else if (alue === "15") {
            let keskivaikea = ArvoLuku(1, 20)
            return keskivaikea;
        } else if (alue === "16") {
            let vaikea = ArvoLuku(1, 50)
            return vaikea;
        }
    }


    /**
     * Kertolaskujen oikeintarkistus
     * Lisää pisteen jos vastaa oikein kaikkiin 3 laskuun.
     */

    OikeatLaskuri = 0
    function TarkistaKertoVastaus() {

        for (let i = 1; i <= 3; i++) {
            let vastaus = Number($(`#vastaus_${[i]}`).val());
            let numero = Number($(`#numero1_${[i]}`).html());
            kertoja = HaeKertoja()

            oikein = numero * kertoja

            if (vastaus === oikein) {
                oikeat++

                $(`input[id=vastaus_${[i]}]`).css("background-color", "greenyellow");
            } else {
                $(`input[id=vastaus_${[i]}]`).css("background-color", "red");
            }
        }
        if (oikeat === 3) {
            OikeatLaskuri++
            $(".tarkista").prop("disabled", true)


        }


        oikeat = 0;
    }







    /** Tarkistaa onko + ja - laskut oikein.
     * Lisää pisteet jos vastaa oikein kaikkiin 3 laskuun.
     */
    let plusjamiinus = ["+", "-"]
    function TarkistaPlusMiinus() {
        for (let i = 1; i <= 3; i++) {
            let vastaus1 = $(`#vastaus_${[i]}`).val();
            let numero1 = Number($(`#numero1_${[i]}`).html());
            let numero2 = Number($(`#numero2_${[i]}`).html());
            let merkki = $(`#lasku${[i]}`).html()
            if (merkki === "+") {
                oikein = numero1 + numero2;
            } else {
                oikein = numero1 - numero2
            }

            if (vastaus1 == oikein) {
                oikeat++

                $(`input[id=vastaus_${[i]}]`).css("background-color", "greenyellow");
            } else {
                $(`input[id=vastaus_${[i]}]`).css("background-color", "red");
            }

        }

        if (oikeat === 3) {
            OikeatLaskuri++
            $(".tarkista").prop("disabled", true)


        }

        oikeat = 0;
    }





    //Laskujen valintafunktio 
    $('[name="button1"]').click(function () {


        switch ($(this).attr("id")) {
            //Kertolaskut
            case "kerto":

                $(".takaisin").show(500);
                $(".kertotaulu").show(500);
                $(".ArvoNumerot").show(500);
                $(".KaikkiLaskut").show(500);
                $(".navigaatio").hide(500);



                /**
                 * jos laskualuetta ei valita. tulee esiin varoitusikkuna.
                 * arvotaan luvut
                 * tulostetaan käyttäjän valitsema kertoja.
                 */
                $("#arvo").click(function () {
                    if (HaeKertoja() === undefined) {
                        $("#varoitus").modal('show');
                    }
                    else {
                        $(".tarkistanappi").show(500);
                        $(".tarkista").prop("disabled", false)
                        $(".kenttä").show(500);
                        //arvotaan joku luku väliltä 1-10. haetaan käyttäjän valitsema kertoja.
                        for (let i = 1; i <= 3; i++) {

                            $(`#numero1_${[i]}`).html(ArvoLuku(1, 10)).show(500);
                            $(`#lasku${[i]}`).html("*");
                            $(`#numero2_${[i]}`).html(HaeKertoja());


                            $("#vastaus_" + [i]).prop("disabled", false);
                            $(`input[id=vastaus_${[i]}]`).css("background-color", "white");
                            $(`input[id=vastaus_${[i]}]`).val("");

                        }
                        $(`input[id=vastaus_1`).select()
                    }
                });
                //vastauksen tarkistus,  näytetään pisteet ja loppupalkinto jos 5 pistettä
                $(".tarkista").click(function () {


                    TarkistaKertoVastaus()
                    $("#arvo").html("Uudet laskut")

                    $(".pisteet").show(500);
                    $("#laskuri").html(`Pisteesi: ${OikeatLaskuri}`);

                    if (OikeatLaskuri === 5) {
                        $("#loppupalkinto").modal('show')
                    }

                });


                break;
                /**
                 * Yhteen- ja vähennyslaskut
                 * käyttäjä voi valita laskualueen.
                 */
            case "yhteen":
                $(".takaisin").show(500);
                $(".tarkista").prop("disabled", false)
                $(".KaikkiLaskut").show(500);
                $(".ArvoNumerot").show(500);
                $(".numeroalue").show(500);
                $(".navigaatio").hide(500);


                /**
                 * arpoo numerot, tuo esille tarkista-napin ja input kentät.
                 *  jos laskualuetta ei valita. tulee esiin varoitusikkuna.
                 */ 

                $("#arvo").click(function () {
                    if (LaskuAlue() === undefined) {
                        $("#varoitus").modal('show');
                    }
                    else {
                        $(".tarkista").prop("disabled", false)
                        $(".kenttä").show(500);
                        $(".tarkistanappi").show(500);
                        /** käyttäjän valitsee laskualueen. 
                         * arvotaan luvut valitun laskualueen mukaan.
                         *  arvotaan onko yhteen- vai vähennyslasku.
                         */ 
                        if (LaskuAlue()) {
                            for (let i = 1; i <= 3; i++) {
                                lasku = getRndInteger(0, 1)
                                $(`#numero1_${[i]}`).html(LaskuAlue());
                                $(`#numero2_${[i]}`).html(LaskuAlue());
                                $(`#lasku${[i]}`).html(plusjamiinus[lasku]);

                            }
                        }
                        // Haetaan arvotut numerot
                        for (let i = 1; i <= 3; i++) {

                            let numero1 = Number($(`#numero1_${[i]}`).html())
                            let numero2 = Number($(`#numero2_${[i]}`).html())
                            let merkki = $(`#lasku${[i]}`).html()
                            // jos vastaus olisi negatiivinen. Vaihtaa numerot toisinpäin
                            if (numero2 > numero1 && merkki === "-") {
                                $(`#numero2_${[i]}`).html(numero1)
                                $(`#numero1_${[i]}`).html(numero2)
                            }
                            // Arvottaessa uudet numerot. Vaihdetaan input-kentät takaisin käytettäviksi
                            $("#vastaus_" + [i]).prop("disabled", false);
                            $(`input[id=vastaus_${[i]}]`).css("background-color", "white");
                            $(`input[id=vastaus_${[i]}]`).val("");
                        }
                        $(`input[id=vastaus_1`).select()
                    }
                });
                //vastauksen tarkistus, näytetään pisteet ja loppupalkinto jos 5 pistettä
                $(".tarkista").click(function () {


                    TarkistaPlusMiinus()
                    $("#arvo").html("Uudet laskut")

                    $(".pisteet").show(500);
                    $("#laskuri").html(`Pisteesi: ${OikeatLaskuri}`);
                    if (OikeatLaskuri === 5) {
                        $("#loppupalkinto").modal('show');
                    }
                });
                break;

            default:
        }





    });

});

